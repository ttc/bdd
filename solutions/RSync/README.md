# The Role-Based Synchronization Approach (RSYNC)

## Solutions

Five different solutions:
* RSyncBDD
	* Select a port while comparing the output cells and its values
	* Try to split with subtrees in an optimal way
	* Merge the Leaves that their is only the minimal number
	* Class for rule: **BddSyncIntegration**
* RSyncBDD-Unordered
	* Match the port order with the subtree order
	* Does not look for an optimal subtree structure
	* Merge the Leaves that their is only the minimal number
	* Class for rule: **BddSyncIntegrationWithoutOrder**
* RSyncBDT
	* Select a port while comparing the output cells and its values
	* Try to split with subtrees in an optimal way
	* Class for rule: **BdtSyncIntegration**
* RSyncBDT-Unordered
	* Match the port order with the subtree order
	* Does not look for an optimal subtree structure
	* Class for rule: **BdtSyncIntegrationWithoutOrder**
* RSyncBDTSync
	* Create the binary decision tree at load time
	* Use construction rule to construct the connected elements (TTandBDTandBDDSyncConstruction)
	* Rule to react on name changes of the TruthTable and BDDs (SyncHeadNamesSync)
	* Rule to react on name changes of Ports (SyncPortNamesSync)
	* Rule that add the Ports in the BDD model if added at the TruthTable element (SyncChangesTruthTableSync)
	* Rule that react on adding of Rows to TruthTable and create new Subtrees and Leaves (SyncChangesTruthTableSync)
