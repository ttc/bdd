package ttc2019

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.{ EObject, EPackage }
import org.eclipse.emf.ecore.xmi.XMLResource
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import scala.collection.JavaConverters._
import java.io.File
import org.eclipse.emf.ecore.EStructuralFeature
import ttc2019.metamodels.create._
import ttc2019.metamodels.tt._
import creation.ICreationTT

/**
 * Simple service to load an ECORE meta and instance model from a file.
 *
 * @author Christopher Werner
 */
class TTCLoader {

  /**
   * Fetches an ecore model from XML.
   *
   * @param path where to find the model
   * @return the model described by the XML
   */  
  def javaOptimizedTTJavaEcore(pathMeta: String, pathInstance: String): TruthTable = {
    require(null != pathMeta && pathMeta.nonEmpty && null != pathInstance && pathInstance.nonEmpty)
    val loader = new LoadEObject
    return loader.loadOptimizedTruthTable(pathMeta, pathInstance)
  }
  
  def loadOptimizedJavaEcore(pathMeta: String, pathInstance: String): EObject = {
    require(null != pathMeta && pathMeta.nonEmpty && null != pathInstance && pathInstance.nonEmpty)
    val loader = new LoadEObject
    return loader.loadOptimized(pathMeta, pathInstance)
  }
  
  def loadSimpleJavaEcore(pathMeta: String, pathInstance: String): EObject = {
    require(null != pathMeta && pathMeta.nonEmpty && null != pathInstance && pathInstance.nonEmpty)
    val loader = new LoadEObject
    return loader.loadSimple(pathMeta, pathInstance)
  }
  
  def loadScalaEcore(pathMeta: String, pathInstance: String): EObject = {
    require(null != pathMeta && pathMeta.nonEmpty && null != pathInstance && pathInstance.nonEmpty)

    val resourceSet = new ResourceSetImpl();
    resourceSet.getResourceFactoryRegistry.getExtensionToFactoryMap.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl())

    
    val ressourceMeta = resourceSet.getResource(URI.createFileURI(pathMeta), true)
    val packageMeta = ressourceMeta.getContents().get(0)

    require(null != ressourceMeta)
    require(!ressourceMeta.getContents.isEmpty)
    
    resourceSet.getPackageRegistry().put("https://www.transformation-tool-contest.eu/2019/tt", packageMeta);
    val ressourceModel = resourceSet.getResource(URI.createURI(pathInstance), true);

    return ressourceModel.getContents().get(0)
  }
  
  def createTruthTableRSYNCInstance(tt: TruthTable, ctts: ICreationTT): Unit = {
    ctts.createTruthTable(tt.getName, null, tt)
    
    tt.getPorts.forEach(p => {
      if (p.isInstanceOf[InputPort]) {
        ctts.createInputPort(p.getName, null, p)
      } else {
        ctts.createOutputPort(p.getName, null, p)
      }
      ctts.createTruthTablePortsPort(tt, p)      
    })
    
    tt.getRows.forEach(r => {
      ctts.createRow(null, r)
      r.getCells.forEach(c => {
        ctts.createCell(c.isValue(), null, c)
        ctts.createCellPortPort(c, c.getPort)
        ctts.createRowCellsCell(r, c)
      })
      ctts.createTruthTableRowsRow(tt, r)
    })
  }

}
