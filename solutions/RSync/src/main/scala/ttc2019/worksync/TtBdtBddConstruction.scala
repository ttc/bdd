package ttc2019.worksync

import org.rosi_project.model_management.core.PlayerSync
import org.rosi_project.model_management.sync.IConstructionCompartment
import org.rosi_project.model_management.sync.roles.IConstructionRole
import org.rosi_project.model_management.sync.roles.ISyncManagerRole

/**
  * Construction Process for Model  BDD and TT.
  */
object TtBdtBddConstruction extends IConstructionCompartment {

  def getConstructorForClassName(classname: Object): IConstructionRole = {
    if (classname.isInstanceOf[sync.tt.TruthTable])
      return new TTTruthTableConstruct()
    if (classname.isInstanceOf[sync.bddg.BDD])
      return new BDDBddConstruct()
    if (classname.isInstanceOf[sync.bdd.BDD])
      return new BDTBddConstruct()
    if (classname.isInstanceOf[sync.tt.InputPort])
      return new TTInputPortConstruct()
    if (classname.isInstanceOf[sync.bddg.InputPort])
      return new BDDInputPortConstruct()
    if (classname.isInstanceOf[sync.bdd.InputPort])
      return new BDTInputPortConstruct()
    if (classname.isInstanceOf[sync.tt.OutputPort])
      return new TTOutputPortConstruct()
    if (classname.isInstanceOf[sync.bddg.OutputPort])
      return new BDDOutputPortConstruct()
    if (classname.isInstanceOf[sync.bdd.OutputPort])
      return new BDTOutputPortConstruct()
    return new Suppressor
  }

  class TTTruthTableConstruct() extends IConstructionRole {

    def construct(comp: PlayerSync, man: ISyncManagerRole): Unit = {
      //Step 1: Get construction values
      val name: String = +this getName()

      //Step 2: Create the object in the other models
      val dBdd = new sync.bddg.BDD(name, Set.empty, null, Set.empty)
      val tBdd = new sync.bdd.BDD(name, null, Set.empty)

      //Step 3: Create Containers 
      createContainerElement(true, true, comp)
      createContainerElement(false, true, dBdd)
      createContainerElement(false, true, tBdd)

      //Step 4: Finish Creation
      makeCompleteConstructionProcess(containers)
    }
  }
  
  class BDDBddConstruct() extends IConstructionRole {

    def construct(comp: PlayerSync, man: ISyncManagerRole): Unit = {
      //Step 1: Get construction values
      val name: String = +this getName()

      //Step 2: Create the object in the other models
      val tt = new sync.tt.TruthTable(name, Set.empty, Set.empty, null)
      val tBdd = new sync.bdd.BDD(name, null, Set.empty)

      //Step 3: Create Containers 
      createContainerElement(true, true, comp)
      createContainerElement(false, true, tt)
      createContainerElement(false, true, tBdd)

      //Step 4: Finish Creation
      makeCompleteConstructionProcess(containers)
    }
  }
  
  class BDTBddConstruct() extends IConstructionRole {

    def construct(comp: PlayerSync, man: ISyncManagerRole): Unit = {
      //Step 1: Get construction values
      val name: String = +this getName()

      //Step 2: Create the object in the other models
      val dBdd = new sync.bddg.BDD(name, Set.empty, null, Set.empty)
      val tt = new sync.tt.TruthTable(name, Set.empty, Set.empty, null)

      //Step 3: Create Containers 
      createContainerElement(true, true, comp)
      createContainerElement(false, true, dBdd)
      createContainerElement(false, true, tt)

      //Step 4: Finish Creation
      makeCompleteConstructionProcess(containers)
    }
  }
  
  class TTInputPortConstruct() extends IConstructionRole {

    def construct(comp: PlayerSync, man: ISyncManagerRole): Unit = {
      //Step 1: Get construction values
      val name: String = +this getName()

      //Step 2: Create the object in the other models
      val dIn = new sync.bddg.InputPort(Set.empty, name, null)
      val tIn = new sync.bdd.InputPort(Set.empty, name, null)

      //Step 3: Create Containers 
      createContainerElement(true, true, comp)
      createContainerElement(false, true, dIn)
      createContainerElement(false, true, tIn)

      //Step 4: Finish Creation
      makeCompleteConstructionProcess(containers)
    }
  }
  
  class BDDInputPortConstruct() extends IConstructionRole {

    def construct(comp: PlayerSync, man: ISyncManagerRole): Unit = {
      //Step 1: Get construction values
      val name: String = +this getName()

      //Step 2: Create the object in the other models
      val ttIn = new sync.tt.InputPort(name, Set.empty, null, null)
      val tIn = new sync.bdd.InputPort(Set.empty, name, null)

      //Step 3: Create Containers 
      createContainerElement(true, true, comp)
      createContainerElement(false, true, ttIn)
      createContainerElement(false, true, tIn)

      //Step 4: Finish Creation
      makeCompleteConstructionProcess(containers)
    }
  }
  
  class BDTInputPortConstruct() extends IConstructionRole {

    def construct(comp: PlayerSync, man: ISyncManagerRole): Unit = {
      //Step 1: Get construction values
      val name: String = +this getName()

      //Step 2: Create the object in the other models
      val dIn = new sync.bddg.InputPort(Set.empty, name, null)
      val ttIn = new sync.tt.InputPort(name, Set.empty, null, null)

      //Step 3: Create Containers 
      createContainerElement(true, true, comp)
      createContainerElement(false, true, dIn)
      createContainerElement(false, true, ttIn)

      //Step 4: Finish Creation
      makeCompleteConstructionProcess(containers)
    }
  }
  
  class TTOutputPortConstruct() extends IConstructionRole {

    def construct(comp: PlayerSync, man: ISyncManagerRole): Unit = {
      //Step 1: Get construction values
      val name: String = +this getName()

      //Step 2: Create the object in the other models
      val dOut = new sync.bddg.OutputPort(Set.empty, name, null)
      val tOut = new sync.bdd.OutputPort(Set.empty, name, null)

      //Step 3: Create Containers 
      createContainerElement(true, true, comp)
      createContainerElement(false, true, dOut)
      createContainerElement(false, true, tOut)

      //Step 4: Finish Creation
      makeCompleteConstructionProcess(containers)
    }
  }
  
  class BDDOutputPortConstruct() extends IConstructionRole {

    def construct(comp: PlayerSync, man: ISyncManagerRole): Unit = {
      //Step 1: Get construction values
      val name: String = +this getName()

      //Step 2: Create the object in the other models
      val ttOut = new sync.tt.OutputPort(name, Set.empty, null, null)
      val tOut = new sync.bdd.OutputPort(Set.empty, name, null)

      //Step 3: Create Containers 
      createContainerElement(true, true, comp)
      createContainerElement(false, true, ttOut)
      createContainerElement(false, true, tOut)

      //Step 4: Finish Creation
      makeCompleteConstructionProcess(containers)
    }
  }
  
  class BDTOutputPortConstruct() extends IConstructionRole {

    def construct(comp: PlayerSync, man: ISyncManagerRole): Unit = {
      //Step 1: Get construction values
      val name: String = +this getName()

      //Step 2: Create the object in the other models
      val dOut = new sync.bddg.OutputPort(Set.empty, name, null)
      val ttOut = new sync.tt.OutputPort(name, Set.empty, null, null)

      //Step 3: Create Containers 
      createContainerElement(true, true, comp)
      createContainerElement(false, true, dOut)
      createContainerElement(false, true, ttOut)

      //Step 4: Finish Creation
      makeCompleteConstructionProcess(containers)
    }
  }
  
  class Suppressor extends IConstructionRole {

    override def construct(comp: PlayerSync, man: ISyncManagerRole): Unit = {
      // just set up the player
      createContainerElement(start=true, con=true, comp)
      makeCompleteConstructionProcess(containers)
    }
  }

}