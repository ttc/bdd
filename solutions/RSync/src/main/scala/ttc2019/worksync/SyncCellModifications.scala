package ttc2019.worksync

import org.rosi_project.model_management.sync.IModificationCompartment
import org.rosi_project.model_management.sync.roles.IModificationRole
import org.rosi_project.model_management.core.PlayerSync

class SyncCellModifications() extends IModificationCompartment {

  def getFirstRole(classname: Object): IModificationRole = {
    if (classname.isInstanceOf[sync.tt.Cell])
      return new Sync()
    return null
  }

  def isFirstIntegration(classname: Object): Boolean = {
    if (classname.isInstanceOf[sync.tt.Cell])
      return true
    return false
  }

  def getNewInstance(): IModificationCompartment = new SyncCellModifications

  class Sync() extends IModificationRole {

    def getOuterCompartment(): IModificationCompartment = SyncCellModifications.this

    /**
     * Rule which add ports to its root in each model.
     */
    def syncSetPort(): Unit = {
      //println("In Sync Set Port")
      val port: sync.tt.Port = +this getPort ()
      if (port.isInstanceOf[sync.tt.OutputPort] && !doSync) {
        doSync = true;
        //get value from cell
        val value: Boolean = +this getValue ()
        //get needed ports
        val opTreePort: PlayerSync = +port getRelatedObject ("sync.bdd.Port")
        //val opDiaPort: PlayerSync = +port getRelatedObject ("sync.bddg.Port")
        //create assignments
        if (opTreePort != null) {
          val o_port = opTreePort.asInstanceOf[sync.bdd.OutputPort]
          val assignment = new sync.bdd.Assignment(value, null, null)

          o_port.addAssignments(assignment)
          assignment.setPort(o_port)
          //trace link
          +this makePlayerSyncRelated (assignment)
        }
        /*if (opDiaPort != null) {
          val o_port = opDiaPort.asInstanceOf[sync.bddg.OutputPort]
          val assignment = new sync.bddg.Assignment(value, null, null)

          o_port.addAssignments(assignment)
          assignment.setPort(o_port)
          //trace link
          +this makePlayerSyncRelated (assignment)
        }*/
        doSync = false;
      }
    }
    
    def syncSetValue(): Unit = {
      if (!doSync) {
        doSync = true;
        var value: Boolean = +this getValue ();
        getSyncer().foreach { a =>
          if (!a.equals(this)) {
            (+a).setValue(value);
          }
        }
        doSync = false;
      }
    }
  }

}