package ttc2019.worksync

import org.rosi_project.model_management.sync.IModificationCompartment
import org.rosi_project.model_management.sync.roles.IModificationRole

/**
  * Synchronization compartment for full name split with space.
  */
class SyncNames() extends IModificationCompartment {

  override def getNextRole(classname: Object): IModificationRole = {
    if (classname.isInstanceOf[sync.bdd.BDD] || classname.isInstanceOf[sync.bddg.BDD] || classname.isInstanceOf[sync.tt.TruthTable])
      return new Sync()
    return null
  }

  def getFirstRole(classname: Object): IModificationRole = {
    if (classname.isInstanceOf[sync.tt.TruthTable])
      return new Sync()
    return null
  }

  override def isNextIntegration(classname: Object): Boolean = {
    if (classname.isInstanceOf[sync.bdd.BDD] || classname.isInstanceOf[sync.bddg.BDD] || classname.isInstanceOf[sync.tt.TruthTable])
      return true
    return false
  }

  def isFirstIntegration(classname: Object): Boolean = {
    if (classname.isInstanceOf[sync.tt.TruthTable])
      return true
    return false
  }

  def getNewInstance(): IModificationCompartment = new SyncNames

  class Sync() extends IModificationRole {

    def getOuterCompartment(): IModificationCompartment = SyncNames.this

    def syncSetName(): Unit = {
      if (!doSync) {
        doSync = true;
        var name: String = +this getName();
        getSyncer().foreach { a =>
          if (!a.equals(this)) {
            (+a).setName(name);
          }
        }
        doSync = false;
      }
    }    
  }

}