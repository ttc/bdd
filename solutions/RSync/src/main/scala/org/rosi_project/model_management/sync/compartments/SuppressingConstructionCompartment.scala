package org.rosi_project.model_management.sync.compartments

import org.rosi_project.model_management.core.PlayerSync
import org.rosi_project.model_management.sync.IConstructionCompartment
import org.rosi_project.model_management.sync.roles.{IConstructionRole, ISyncManagerRole}

/** An [[IConstructionCompartment]] which will not create any related instances in other models
  *
  * @author Rico Bergmann
  */
object SuppressingConstructionCompartment extends IConstructionCompartment {

  override def getConstructorForClassName(classname: Object): IConstructionRole = new Suppressor
  
  /** The constructor will only create the necessary `plays` relationships with the synchronization
    * services
    */
  class Suppressor extends IConstructionRole {

    override def construct(comp: PlayerSync, man: ISyncManagerRole): Unit = {
      // just set up the player
      createContainerElement(start=true, con=true, comp)
      makeCompleteConstructionProcess(containers)
    }

  }

}
