package org.rosi_project.model_management.sync.roles

import org.rosi_project.model_management.sync.IModificationCompartment

/**
  * Interface for the synchronization roles.
  */
trait IModificationRole {
  
  /**
    * Function to get the synchronization compartment from a role instance.
    */
  def getOuterCompartment: IModificationCompartment
}
