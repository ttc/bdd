package org.rosi_project.model_management.sync

import org.rosi_project.model_management.sync.roles.IExtensionRole
import scroll.internal.Compartment

/**
  * Interface for each extension rule.
  */
trait IExtensionCompartment extends Compartment {
  
  /**
    * Return a role instance that handles the extension process for the object.
    */
  def getExtensionForClassName(classname: Object) : IExtensionRole
  
  /**
   * Return on default the simple name of the class.
   */
  def getExtensionName() : String = this.getClass.getSimpleName
}