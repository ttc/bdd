package org.rosi_project.model_management.sync.roles

import org.rosi_project.model_management.sync.helper.TransformationContainer
import org.rosi_project.model_management.core.PlayerSync

/**
  * Interface for the integration roles.
  */
trait ITransformationRole {

  /**
   * Container list for the integration process.
   */
  protected var containers: Set[TransformationContainer] = Set.empty

  /**
   * Create a container element with the incoming configuration.
   */
  protected def createContainerElement(newPlayer: PlayerSync, oldPlayer: PlayerSync): Unit = {
    if (newPlayer == null || oldPlayer == null)
      return
    containers += new TransformationContainer(newPlayer, oldPlayer)
  }
  
  /**
   * General integration function for external call.
   */
  def transform(comp: PlayerSync) : PlayerSync
}