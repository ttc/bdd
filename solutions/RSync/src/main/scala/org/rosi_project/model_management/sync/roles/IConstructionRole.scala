package org.rosi_project.model_management.sync.roles

import org.rosi_project.model_management.core.PlayerSync
import org.rosi_project.model_management.sync.helper.ConstructionContainer

/**
  * Interface for the constructor roles.
  */
trait IConstructionRole {

  /**
    * Container list for the construction process.
    */
  protected var containers: Set[ConstructionContainer] = Set.empty

  /**
    * Create a container element with the incoming configuration.
    */
  protected def createContainerElement(start: Boolean, con: Boolean, play: PlayerSync): Unit = {
    require(play != null)
    containers += new ConstructionContainer(start, con, play)
  }

  /**
    * General construction function for external call.
    */
  def construct(comp: PlayerSync, man: ISyncManagerRole): Unit
}
