package org.rosi_project.model_management.sync.roles

/**
  * Interface for the destructor roles.
  */
trait IDestructionRole {
  
  /**
    * General destruction function for external call.
    */
  def deleteRoleFunction(): Unit
}
