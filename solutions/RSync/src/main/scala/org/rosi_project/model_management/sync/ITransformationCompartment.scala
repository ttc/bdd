package org.rosi_project.model_management.sync

import scroll.internal.Compartment
import org.rosi_project.model_management.sync.roles.IModificationRole
import org.rosi_project.model_management.sync.roles.ITransformationRole
import org.rosi_project.model_management.sync.helper.TransformationContainer
import org.rosi_project.model_management.core._
import org.rosi_project.model_management.sync.roles.ISyncManagerRole

/**
 * Interface for each transformation rule.
 */
trait ITransformationCompartment extends Compartment {

  /**
   * Return a role instance that handles the transformation process for a new model to this instance.
   */
  def getTransformation(classname: Object): ITransformationRole

  /**
   * Return a role instance that handles the transformation process for a new relational compartment.
   */
  def getRelationTransformation(classname: Object): ITransformationRole = null
  
  /**
   * Return a role instance that handles the transformation process for a new model to this instance.
   * Can only be called in this transformation acts as sub transformation.
   */
  protected def getSubTransformation(classname: Object): ITransformationRole = null

  /**
   * Function to create special connection add the end of the transformation.
   */
  def finishFunction(): Unit

  protected def connectTargetElementWithSourceElements(target: PlayerSync, sourceList: Set[PlayerSync]): Unit = {
    var containers: Set[TransformationContainer] = Set.empty
    //Create Containers
    sourceList.foreach(e => {
      containers += new TransformationContainer(target, e)
    })
    //Finish Creation
    makeCompleteIntegrationProcess(containers)
  }

  protected def connectTargetElementWithSourceElement(target: PlayerSync, source: PlayerSync): Unit = {
    var containers: Set[TransformationContainer] = Set.empty
    //Create Container
    containers += new TransformationContainer(target, source)
    //Finish Creation
    makeCompleteIntegrationProcess(containers)
  }

  private def addExtensionRoles(containers: Set[TransformationContainer]): Unit = {
    containers.filter(_.newManagerConnection).foreach { cc =>
      ConsistencyManagement.getExtensions().foreach { e =>
        var role = e.getExtensionForClassName(cc.getNewPlayerInstance())
        if (role != null) {
          cc.getNewManagerInstance() play role
        }
      }
    }
  }

  private def notifyExtensionRoles(containers: Set[TransformationContainer]): Unit = {
    if (!ConsistencyManagement.getExtensions().isEmpty) {
      containers.filter(_.newManagerConnection).foreach { cc =>
        var playerInstance = cc.getNewPlayerInstance()
        +playerInstance insertNotification ()
      }
    }
  }

  /**
   * Add Manager roles to all constructed elements.
   */
  private def addManagerRoles(containers: Set[TransformationContainer]): Unit = {
    containers.filter(_.newManagerConnection).foreach { cc =>
      cc.getNewPlayerInstance() play cc.getNewManagerInstance()
    }
  }

  /**
   * Add the delete roles for the elements in the TransformationContainer.
   */
  private def addDeleteRoles(containers: Set[TransformationContainer]): Unit = {
    containers.filter(_.newManagerConnection).foreach { cc =>
      cc.getNewManagerInstance() play ConsistencyManagement.getDestructionRule().getDestructorForClassName(cc.getNewPlayerInstance())
    }
  }

  /**
   * Add the related RoleManagers for the elements in the TransformationContainer.
   */
  private def addRelatedRoleManager(containers: Set[TransformationContainer]): Unit = {
    containers.foreach { cc =>
      val oldPlayer = cc.getOldPlayerInstance()
      if (cc.simpleRelatedManagerConnection) {
        val manager: ISyncManagerRole = +oldPlayer getManager ()
        if (manager != null) {
          manager.makeRelated(cc.getNewManagerInstance())
        }
      } else {
        val allManager: Set[ISyncManagerRole] = +oldPlayer getAllManager ()
        if (allManager != null) {
          allManager.foreach { r =>
            r.makeRelated(cc.getNewManagerInstance())
          }
        }
      }
    }
  }

  /**
   * Combine the ConsistencyManagement with all Players from the TransformationContainer.
   */
  private def synchronizeCompartments(containers: Set[TransformationContainer]): Unit = {
    containers.filter(_.newManagerConnection).foreach { cc =>
      ConsistencyManagement combine cc.getNewPlayerInstance()
    }
  }

  /**
   * Create the Synchronization mechanisms for the elements in the TransformationContainer.
   */
  private def bindSynchronizationRules(containers: Set[TransformationContainer]): Unit = {
    containers.filter(_.newManagerConnection).foreach { cc =>
      val oldPlayer = cc.getOldPlayerInstance()
      val allManager: Set[ISyncManagerRole] = +oldPlayer getAllManager ()
      if (allManager != null) {
        allManager.foreach { rm =>
          val roles = rm.roles()
          //println("Player: " + rm.player + "Roles: " + roles)
          roles.foreach { r =>
            if (r.isInstanceOf[IModificationRole]) {
              val syncRole: IModificationRole = r.asInstanceOf[IModificationRole]
              val syncComp: IModificationCompartment = syncRole.getOuterCompartment
              //println("+~~~Sync: " + syncRole + " " + syncComp)
              if (!syncComp.containsSyncer(cc.getNewPlayerInstance()) && syncComp.isFirstIntegration(r.player.right.get)) {
                val newRole = syncComp.getNextIntegrationRole(cc.getNewPlayerInstance())
                //println("+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~New Role: " + newRole)
                if (newRole != null) {
                  cc.getNewManagerInstance() play newRole
                  allManager.foreach { internalSync =>
                    val playerSync = internalSync.player.right.get
                    if (syncComp.isNextIntegration(playerSync)) {
                      if (!syncComp.containsSyncer(playerSync)) {
                        internalSync play syncComp.getNextIntegrationRole(playerSync)
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * Fill the test lists with all Players from the TransformationContainer.
   */
  private def fillTestLists(containers: Set[TransformationContainer]): Unit = {
    containers.filter(_.newManagerConnection).foreach { cc =>
      ModelElementLists.addElement(cc.getNewPlayerInstance())
    }
  }

  /**
   * Do the integration process automatically.
   */
  private def makeCompleteIntegrationProcess(containers: Set[TransformationContainer]): Unit = {
    containers.foreach(cc => {
      if (cc.getNewManagerInstance() == null) {
        val newPlayer = cc.getNewPlayerInstance()
        val manager = +newPlayer getManager ()
        if (manager.isRight && manager.right.get != null) {
          cc.newManagerConnection = false
          cc.newManagerInstance = manager.right.get
        } else {
          cc.newManagerConnection = true
          cc.newManagerInstance = ConsistencyManagement.createRoleManager()
        }
      }
    })

    //add new role managers to the new players
    this.addManagerRoles(containers)
    //this.addDeleteRoles(containers)
    this.addRelatedRoleManager(containers)
    //combines the new compartments with the existing ones
    this.synchronizeCompartments(containers)
    this.bindSynchronizationRules(containers)
    //add extension roles and notify them because of creation
    this.addExtensionRoles(containers)
    this.notifyExtensionRoles(containers)
    //add the new model element to the elements list
    this.fillTestLists(containers)
    /*println("Integrate +++++++++++++++++++++++++++++++++++++++++++++---------------------------------------------+++++++++++++++++++++++++++++++++++++++++++++++++++")
    containers.foreach { cc =>
      println((cc.getNewPlayerInstance()).roles())
      println((cc.getOldPlayerInstance()).roles())
    }
    println("Integrate ++++++++++++++++++++++++++++++++++++++++++++------------------------++++++++++++++++++++++++++++++++++++++++++++++++++++")*/
  }
}
