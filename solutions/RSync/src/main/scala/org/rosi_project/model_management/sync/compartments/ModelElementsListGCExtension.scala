package org.rosi_project.model_management.sync.compartments

import org.rosi_project.model_management.core.ModelElementLists
import org.rosi_project.model_management.sync.IExtensionCompartment
import org.rosi_project.model_management.sync.roles.IExtensionRole
import scroll.internal.errors.SCROLLErrors.TypeError

/** Extension to delete instances which are removed from the synchronization context from the
  * [[ModelElementLists]]. This extension therefore functions as some kind of garbage collector,
  * hence the name.
  *
  * @author Rico Bergmann
  */
object ModelElementsListGCExtension extends IExtensionCompartment {

  /**
    * Return a role instance that handles the extension process for the object.
    */
  override def getExtensionForClassName(classname: Object): IExtensionRole = new GarbageCollector

  /** The actual extension.
    */
  class GarbageCollector extends IExtensionRole {

    /**
      * Function to react on insertion behavior.
      */
    override def notifyInsertion(): Unit = {
      ;
    }

    /**
      * Function to react on deletion behavior.
      */
    override def notifyDeletion(): Unit = {      
      val player: Either[TypeError, AnyRef] = this.player

      player.right.foreach { obj =>
        println(s"Removing $obj")
        ModelElementLists.removeElement(obj)
      }

    }

    /**
      * Function to react on update behavior.
      */
    override def notifyUpdate(): Unit = {
      ;
    }
  }

}
