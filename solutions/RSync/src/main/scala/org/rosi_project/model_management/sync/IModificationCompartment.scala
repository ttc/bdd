package org.rosi_project.model_management.sync

import org.rosi_project.model_management.sync.roles.IModificationRole
import scroll.internal.Compartment

import scala.collection.immutable.Set

/**
  * Interface for each synchronization rule.
  */
trait IModificationCompartment extends Compartment {

  /**
    * Variable to proof if he is actual in a sync process.
    */
  protected var doSync = false
  /**
    * All sync roles of this synchronization rule.
    */
  private var syncer: Set[IModificationRole] = Set.empty

  /**
    * Get roles for all integration classes.
    */
  protected def getNextRole(classname: Object) : IModificationRole = {
    getFirstRole(classname)
  }
  
  /**
    * Get roles for integration classes. Should give less roles than getNextRole.
    */
  protected def getFirstRole(classname: Object) : IModificationRole
  
  def containsSyncer(value: Object): Boolean = {
    syncer.foreach { s =>
      if (+s == +value) {
      //if (s.player.equals(value.player)) {
        return true;
      }
    }
    return false
  }

  def addSyncer(sync: IModificationRole): Unit = {
    syncer += sync
  }

  /**
    * Get the list of all sync roles.
    */
  def getSyncer(): Set[IModificationRole] = syncer

  /**
    * Clear the list of all sync roles.
    */
  def clearSyncer(): Unit = {
    syncer = Set.empty
  }

  /**
    * Get roles for integration classes. Should give less roles than getNextRole.
    */
  def getFirstIntegrationRole(classname: Object) : IModificationRole = {
    val role: IModificationRole = this.getFirstRole(classname)
    if (role != null)
      this.addSyncer(role)
    role
  }

  /**
    * Get all roles for integration classes.
    */
  def getNextIntegrationRole(classname: Object) : IModificationRole = {
    val role: IModificationRole = this.getNextRole(classname)
    if (role != null)
      this.addSyncer(role)
    role
  }

  /**
    * Get roles for all integration classes.
    */
  def isFirstIntegration(classname: Object): Boolean

  /**
    * Get boolean if next integration
    */
  def isNextIntegration(classname: Object): Boolean = {
    isFirstIntegration(classname)
  }

  /**
    * Create a new instance of this class.
    */
  def getNewInstance: IModificationCompartment

  /**
   * Return on default the simple name of the class.
   */
  def getRuleName: String = this.getClass.getSimpleName
}
