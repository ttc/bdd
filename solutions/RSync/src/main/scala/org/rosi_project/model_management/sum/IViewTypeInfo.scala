package org.rosi_project.model_management.sum

import org.rosi_project.model_management.sum.join.IJoinInfo
import org.rosi_project.model_management.core.RsumManagement

trait IViewTypeInfo {
  
  /**
   * Return on default the simple name of the class.
   */
  def getViewName: String = this.getClass.getSimpleName
  
  protected def getNewViewTypeInstance(): IViewCompartment = {
    RsumManagement.addViewTypeInfo(this)
    var view = RsumManagement.getActiveViewFromName(this.getViewName)
    if (view == null) {
      return RsumManagement.addActiveView(this.getNewInstance())
    }
    else {
      return view
    }
  }
  
  def getJoinInfos(): Set[IJoinInfo]
  
  private[model_management] def getViewRsumInstance(): IViewCompartment = getNewInstance
  
  protected def getNewInstance(): IViewCompartment
}